package com.fundamentos;

import java.util.Scanner;

public class TrabajoFinal {

    static String[] SesionDeYoga = {"Carmen", "Roxana", "Ana", "Carlos", "Antony", "Olga", "Claudia", "Dayana", "Carlota"};
    static String[] SesionDeCrossfit = {"Susana", "Cinthia", "Ester", "Andres", "Saul", "Luis", "Lili", "Alejandra", "Julia"};
    static String[] SesionDeCalentamiento = {"Alina", "Andrea", "Sandy", "Ruben", "Renzo", "Edgar", "Fernando", "Rubio", "Hans"};
    static String[] SesionDeResistencia = {"Juan", "Javier", "Pablo", "Raul", "Sara", "Luicin", "Julio", "Lucas", "Cesar"};
    static String[] SesionDeCardiovascular = {"Pedro", "Jennifer", "Carolina", "Simeon", "Violeta", "Abigail", "Keyla", "Laura", "Veronika"};

    static String[] EntrenadoresHorarioA = {"Elizabeth", "Darío", "Fernando", "Brenda", "Maria", "Miguel",
            "Ruben", "Roberto","Jorge","Ronald","Robert","Marcos","Vicente","José","Eduardo","Felipe"};
    static int[] horarioEntrenamientoA = {7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21};
    static String[] EntrenadoresHorarioB = {"Diego", "Paula", "Carlos", "Roxana", "Jennifer", "Juan", "Paolo",
            "Alexandra","Angelo","Bruno","Martin","Brayan","Karina","Omar","Mateo"};
    static int[] horarioEntrenamientoB = {7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double temperaturaCorporalCliente;
        String mensaje1, zonaGym, mensaje2, tipoHorarioEntrenador, horario,horarioEntradaZona;
        int  horarioEntrenamiento;
        do {
            System.out.print("Hola, bienvenido a Fitness Gym\n¿Cómo amaneciste hoy?\nIngrese su temperatura corporal: ");
            temperaturaCorporalCliente = sc.nextDouble();
            mensaje1 = validarControlDeIngresoClientes(temperaturaCorporalCliente);
            System.out.println(mensaje1);
        }while(temperaturaCorporalCliente>37.5);
        System.out.print("\"Tenemos estas áreas disponibles:\nestiramiento, cardiovascular y resistencia \n¿A qué zona del Gym se dirige? ");
        zonaGym = sc.next();
        System.out.print("¡Que bien! Tenemos horarios desde las 09:00 horas hasta 22:00 horas, ingrese el horario de entrada de la zona: ");
        horarioEntradaZona = sc.next();
        mensaje2 = controlarAforoPorZonaPorTiempo(zonaGym, horarioEntradaZona);
        System.out.println(mensaje2);
        System.out.print("\nSolicitar horario de entrenadores \nA: Deportistas Profesionales. \nB: Deportistas Amateurs. ");
        System.out.print("\nQué tipo de horario de entrenadores solicita: ");
        tipoHorarioEntrenador = sc.next();
        System.out.print("Ingrese solo la hora de entrenamiento para destinarte un entrenador calificado : ");
        horarioEntrenamiento = sc.nextInt();
        horario = solicitarEntrenadoresPorTipoHorario(tipoHorarioEntrenador, horarioEntrenamiento);
        System.out.println(horario);
    }
    public static String validarControlDeIngresoClientes(double temperaturaCorporalCliente) {
        Scanner sc = new Scanner(System.in);
        String mensaje, cliente, nombreDeSesion = null;
        String[] Respuestas = new String[4];
        int contador = 0;

        if (temperaturaCorporalCliente <= 37.5) {
            mensaje ="Adelante, puedes ingresar al gimnasio. \nNo olvides tus implementos de bioseguridad.\n";
        } else {
            System.out.print("Hoy estás un poco caliente, ingrese su nombre: ");
            cliente = sc.next();
            for (int j = 0; j < 9; j++) {
                if (SesionDeYoga[j].equals(cliente)) {
                    nombreDeSesion = "YOGA";
                } else if (SesionDeCrossfit[j].equals(cliente)) {
                    nombreDeSesion = "CROSSFIT";
                } else if (SesionDeCalentamiento[j].equals(cliente)) {
                    nombreDeSesion = "CALENTAMIENTO";
                } else if (SesionDeResistencia[j].equals(cliente)){
                    nombreDeSesion = "RESISTENCIA";
                } else if (SesionDeCardiovascular[j].equals(cliente)) {
                    nombreDeSesion = "CARDIOVASCULAR";
                }
            }
            System.out.println("\n" + cliente + ", al tener una temperatura superior a 37.5°:  \nNo podrás ingresar a las instalaciones, para ello se reprogramará su sesión de "
                    + nombreDeSesion + "\nAsimismo, deberás responder con si o no el test de síntomas covid.");

            System.out.println("\n----En los últimos 15 días: ----\n");
            System.out.print("¿Ha tenido fiebre? ");
            Respuestas[0] = sc.next();
            System.out.print("¿Ha sentido la pérdida del olfato? ");
            Respuestas[1] = sc.next();
            System.out.print("¿Ha sentido la pérdida del gusto? ");
            Respuestas[2] = sc.next();
            System.out.print("¿Ha estado en contacto con personas contagiadas con covid-19? ");
            Respuestas[3] = sc.next();

            for (int i = 0; i < Respuestas.length; i++) {
                if (Respuestas[i].equals("no")) {
                    contador++;
                }
            }
            if (contador == 4) {
                mensaje = "\nSe reprogramará  su sesión de " + nombreDeSesion + " dentro de 3 dias.\n";
            } else {
                mensaje = "\nSe reprogramará su sesión de " + nombreDeSesion + " dentro de 15 dias.\nEsperamos su pronta recuperación.\n";
            }
        }
        return mensaje;
    }

    public static String controlarAforoPorZonaPorTiempo(String zona, String horarioIngresoZona) {
        String mensaje = null;
        int minSalidaZona, horaSalidaZona;
        int minIngresoZona=Integer.parseInt( horarioIngresoZona.substring(3,5));
        int horaIngresoZona=Integer.parseInt( horarioIngresoZona.substring(0,2));

        switch (zona) {
            case "estiramiento":
                for (int i = 1; i <= 15; i++) {
                    minIngresoZona = minIngresoZona + 1;
                    if (minIngresoZona >= 60) {
                        minSalidaZona = minIngresoZona - 60;
                        horaSalidaZona = horaIngresoZona + 1;
                    } else {
                        minSalidaZona = minIngresoZona;
                        horaSalidaZona = horaIngresoZona;
                    }
                    if (minSalidaZona<10){
                        mensaje = "Su horario de salida de la zona estiramiento es " + horaSalidaZona + ":" +"0" + minSalidaZona + " .";
                    }else {
                        mensaje = "Su horario de salida de la zona estiramiento es " + horaSalidaZona + ":" + minSalidaZona + " .";
                    }
                }
                break;
            case "cardiovascular":
                for (int i = 1; i <= 30; i++) {
                    minIngresoZona = minIngresoZona + 1;
                    if (minIngresoZona >= 60) {
                        minSalidaZona = minIngresoZona - 60;
                        horaSalidaZona = horaIngresoZona + 1;
                    } else {
                        minSalidaZona = minIngresoZona;
                        horaSalidaZona = horaIngresoZona;
                    }
                    if (minSalidaZona<10){
                        mensaje = "Su horario de salida de la zona cardiovascular es " + horaSalidaZona + ":" +"0" + minSalidaZona + " .";
                    }else {
                        mensaje = "Su horario de salida de la zona cardiovascular es " + horaSalidaZona + ":" + minSalidaZona + " .";
                    }
                }
                break;
            case "resistencia":
                for (int i = 1; i <= 30; i++) {
                    minIngresoZona = minIngresoZona + 1;
                    if (minIngresoZona >= 60) {
                        minSalidaZona = minIngresoZona - 60;
                        horaSalidaZona = horaIngresoZona + 1;
                    } else {
                        minSalidaZona = minIngresoZona;
                        horaSalidaZona = horaIngresoZona;
                    }
                    if (minSalidaZona<10){
                        mensaje = "Su horario de salida de la zona resistencia es " + horaSalidaZona + ":" +"0" + minSalidaZona + " .";
                    }else {
                        mensaje = "Su horario de salida de la zona resistencia es " + horaSalidaZona + ":" + minSalidaZona + " .";
                    }
                }
                break;
        }
        return mensaje;
    }

    public static String solicitarEntrenadoresPorTipoHorario(String tipoHorarioEntrenador, int horaEntrenamiento) {
        String mensaje = null, entrenador = null;
        int horario = 0;
        switch (tipoHorarioEntrenador) {
            case "A":
                for (int a = 0; a < horarioEntrenamientoA.length; a++) {
                    if (horarioEntrenamientoA[a] == horaEntrenamiento) {
                        horario = horarioEntrenamientoA[a];
                        entrenador = EntrenadoresHorarioA[a];
                    }
                }
                mensaje = "Está a su disposición el personal trainer " + entrenador + " a partir del horario de las " + horario + ":00 .";
                break;
            case "B":
                for (int b = 0; b < horarioEntrenamientoB.length; b++) {
                    if (horarioEntrenamientoB[b] == horaEntrenamiento) {
                        horario = horarioEntrenamientoB[b];
                        entrenador = EntrenadoresHorarioB[b];
                    }
                }
                mensaje = "Está a su disposición el personal trainer " + entrenador + " a partir del horario de las " + horario + ":00 .";
                break;
        }return mensaje;
    }
}
